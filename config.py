import os
import sys

#// gcc ==================================================
CC          =  'arm-linux-gnueabihf-gcc'
CXX         =  'arm-linux-gnueabihf-g++'
AS          =  'arm-linux-gnueabihf-gcc'
AR          =  'arm-linux-gnueabihf-ar'
LINK        =  'arm-linux-gnueabihf-gcc'
OBJDUMP     =  'arm-linux-gnueabihf-objdump'
OBJCPY      =  'arm-linux-gnueabihf-objcopy'
STRIP       =  'arm-linux-gnueabihf-strip'


#// path =================================================
ROOT        = os.getcwd()
LIBPATH     = ROOT + '/lib'
SRC_DIR     = ROOT + '/src'

OUT_DIR     = ROOT + '/build'
BIN_DIR     = OUT_DIR + '/bin'
RELEASE_DIR = OUT_DIR + '/release'
PUB_DIR     = OUT_DIR + '/public'

#// out file 
ELF_FILE    = BIN_DIR + '/adv7280a.elf'


#// shared library =======================================
#LIBS        = [ 'stdc++', 'm', 'rt', 'pthread']
LIBS        = [           'm', 'rt', 'pthread']


#// define ===============================================
CFLAGS       = ' -std=gnu99 -O2 '

CCFLAGS      = ' ' 
CCFLAGS     += ' -D_GNU_SOURCE '

CXXFLAGS    = ' '

ASFLAGS     = ' '
ARFLAGS     = ' -rsv '

LDFLAGS     = ' '

