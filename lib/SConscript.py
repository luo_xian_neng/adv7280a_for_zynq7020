import os
import sys
Import('env')

OBJS  = []
OBJS += Glob('*.a')

cwd = os.getcwd()
list = os.listdir(cwd)
for d in list:
    path = os.path.join(cwd, d)
    if os.path.isfile(os.path.join(path, 'SConscript.py')):
        OBJS = OBJS + SConscript(os.path.join(d, 'SConscript.py'))

Return('OBJS')
