import os
import sys
import config

Execute(Mkdir(config.OUT_DIR))
Execute(Mkdir(config.BIN_DIR))
Execute(Mkdir(config.RELEASE_DIR))
Execute(Mkdir(config.PUB_DIR))

ROOT = os.getcwd()
Export('ROOT')

#// include ==============================================
INCLUDES = []
list = os.listdir(ROOT)
for dir in list:
    path = os.path.join(ROOT, dir)
    if os.path.isfile(os.path.join(path, 'include.py')):
        INCLUDES += Split(path)
        INCLUDES += SConscript(os.path.join(dir, 'include.py'))
#print 'path:', INCLUDES


#// Environment ==========================================
env = Environment(ENV  = os.environ,      
		CPPPATH= INCLUDES,	
		CFLAGS = config.CFLAGS,
	        CC     = config.CC, 		CCFLAGS   = config.CCFLAGS,
	        CXX    = config.CXX, 		CXXFLAGS  = config.CXXFLAGS,
	        AS     = config.AS, 		ASFLAGS   = config.ASFLAGS,
	        AR     = config.AR, 		ARFLAGS   = config.ARFLAGS,
	        LINK   = config.LINK,		LINKFLAGS = config.LDFLAGS)
env.AppendENVPath('LIBPATH', config.LIBPATH)
env.Replace(LIBS=  config.LIBS)
env['LINKCOM']    = '$LINK -o $TARGET $LINKFLAGS $__RPATH $_LIBDIRFLAGS -Wl,"-(" $SOURCES $_LIBFLAGS -Wl,"-)"'
env['CCCOMSTR']   = '$CC -o $TARGET $SOURCES'
env['CXXCOMSTR']  = '$CXX -o $TARGET $SOURCES'
env['ARCOMSTR']   = '$AR -o $TARGET'
env['LINKCOMSTR'] = '$LINK -o $TARGET'
Export('env')
#for item in env.Dictionary():
#	print '--(%s:%s)' % (item, env[item])

OBJS = []
list = os.listdir(ROOT)
for d in list:
    path = os.path.join(ROOT, d)
    if os.path.isfile(os.path.join(path, 'SConscript.py')):
        OBJS = OBJS + SConscript(os.path.join(d, 'SConscript.py'))


#// output file
env.Program(config.ELF_FILE,     OBJS)
env.Install(config.RELEASE_DIR,  config.ELF_FILE)

