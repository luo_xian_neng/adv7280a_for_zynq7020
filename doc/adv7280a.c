#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <errno.h>

#if __linux__
#include <unistd.h>
#include <termios.h>
#endif
#include "printf.h"
#include "adv7280a.h"
#include "axi_dev.h"

#ifndef ADV7280A_TABLE_SIZE
#define ADV7280A_TABLE_SIZE	(22)
#endif

#ifndef ADV7280A_MMAP_SIZE
#define ADV7280A_MMAP_SIZE 0x1000
#endif

#ifndef ADV7280A_BASE_ADDR
#define ADV7280A_BASE_ADDR 0x41200000
#endif

typedef struct _ADV7280AHndl
{
	int		fd;
	TXDev	adv7280aDev;
}ADV7280AHndl;

//adv7280a 驱动初始化表
static const uint8_t _adv7280aInitTable[ADV7280A_TABLE_SIZE][4] =
{
	{0x42, 0x0F, 0x80, 0},
	//usleep 10ms
	{0x42, 0x0F, 0x00, 0},
	{0x42, 0x52, 0xCD, 0},
	{0x42, 0x00, 0x00, 0},
	{0x42, 0x0E, 0x80, 0},
	{0x42, 0x9C, 0x00, 0},
	{0x42, 0x9C, 0xFF, 0},
	{0x42, 0x0E, 0x00, 0},
	{0x42, 0x80, 0x51, 0},
	{0x42, 0x81, 0x51, 0},
	{0x42, 0x82, 0x68, 0},
	{0x42, 0x6a, 0x03, 0},
	{0x42, 0x6B, 0x91, 0},
	{0x42, 0x17, 0x41, 0},
	{0x42, 0x03, 0x0C, 0},
	{0x42, 0x04, 0x07, 0},
	{0x42, 0x13, 0x00, 0},
	{0x42, 0x1D, 0x40, 0},
	{0x42, 0xFD, 0x84, 0},
	{0x84, 0xA3, 0x00, 0},
	{0x84, 0x5B, 0x00, 0},
	{0x84, 0x55, 0x80, 0},
};

//adv7280a FPGA寄存器地址
typedef enum _adv7280aRegAddr
{
	ADV7280A_RESET		= 0x28,
	ADV7280A_POWERDOWN	= 0x2c
}adv7280aRegAddr;

static inline void adv7280aRegWrite(TXDev *handle, uint32_t addr, uint32_t val)
{
	handle->pmem[addr >> 2] = val;
	barrier();
}

static int _adv7280aWrite(int fd, const uint8_t *buf)
{
	if (buf == NULL)
	{
		dbg_error("_adv7280aWrite buf is NULL\n");
		return -1;
	}
	
	int ret = write(fd, buf, 4);
	if (ret < 0)
	{
		dbg_error("_adv7280aWrite errorno = %d ret=%d\n", errno, ret);
		return -1;
	}

	return 0;
}

int adv7280aInit(void)
{
	int fd;
	int ret = 0;
	
	//先初始 adv7280a 相关的FPGA寄存器
	TXDev adv7280aDev;

	xdev_init_ex(&adv7280aDev, ADV7280A_BASE_ADDR, ADV7280A_MMAP_SIZE, "adv7280a dev");

	adv7280aRegWrite(&adv7280aDev, ADV7280A_RESET, 0);
	adv7280aRegWrite(&adv7280aDev, ADV7280A_POWERDOWN, 0);

	usleep(10000);

	adv7280aRegWrite(&adv7280aDev, ADV7280A_POWERDOWN, 1);

	usleep(10000);
	
	adv7280aRegWrite(&adv7280aDev, ADV7280A_RESET, 1);

	//初始 adv7280a 设备驱动
	fd = open("/dev/jimu.adv7280a", O_RDWR);
	if (fd < 0)
	{
		dbg_error("open(/dev/jimu.adv7280a)");
		return -1;
	}

	usleep(10000);
	for (int i = 0; i < 1; i++)
	{
		_adv7280aWrite(fd, _adv7280aInitTable[i]);
	}

	usleep(10000);
	for (int i = 1; i < ADV7280A_TABLE_SIZE; i++)
	{
		ret |= _adv7280aWrite(fd, _adv7280aInitTable[i]);
	}

	xdev_uninit(&adv7280aDev);
	close(fd);
	
	return ret;
}

void *adv7280aOpen(void)
{
	ADV7280AHndl *adv7280aHndl = (ADV7280AHndl *)malloc(sizeof(ADV7280AHndl));

	if (NULL == adv7280aHndl)
	{
		return NULL;
	}

	if (xdev_init_ex(&adv7280aHndl->adv7280aDev, ADV7280A_BASE_ADDR, ADV7280A_MMAP_SIZE, "adv7280a dev") < 0)
	{
		free(adv7280aHndl);
		adv7280aHndl = NULL;
		return NULL;
	}

	adv7280aHndl->fd = open("/dev/jimu.adv7280a", O_RDWR);

	if (adv7280aHndl->fd < 0)
	{
		free(adv7280aHndl);
		adv7280aHndl = NULL;
		return NULL;
	}

	return adv7280aHndl;
}

void adv7280aClose(void *phandle)
{
	if (NULL == phandle)
	{
		dbg_error("adv7280aClose handle is NULL\n");
		return;
	}

	ADV7280AHndl *adv7280aHndl = (ADV7280AHndl *)phandle;

	xdev_uninit(&adv7280aHndl->adv7280aDev);
	close(adv7280aHndl->fd);

	return;
}

int adv7280aWrite(void *phandle, const uint8_t *pBuf, int size)
{
	if (NULL == phandle)
	{
		dbg_error("adv7280aClose handle is NULL\n");
		return -1;
	}

	if (NULL == pBuf)
	{
		dbg_error("adv7280aClose write buffer is NULL\n");
		return -1;
	}

	if (size <= 0 || (size & 0x3))
	{
		dbg_error("adv7280aClose write size is 4 multiple...\n");
		return -1;
	}

	ADV7280AHndl *adv7280aHndl = (ADV7280AHndl *)phandle;
	uint8_t *pTmpBuf = pBuf;
	int ret = 0;
	for (int i = 0; i < size / 4; i++)
	{
		ret |= _adv7280aWrite(adv7280aHndl->fd, pTmpBuf);
		pTmpBuf += 4;
	}

	return ret;
}