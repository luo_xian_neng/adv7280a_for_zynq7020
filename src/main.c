/***********************************************************************************************//**
*\n  @file       main.c
*\n  @brief      zynq产品usb串口升级方案
*\n  @details
*\n -----------------------------------------------------------------------------------
*\n  文件说明：
*\n       1. zynq产品usb串口升级方案
*\n       3. 嵌入式linux程序，只考虑采用gcc编译。
*\n       4. 守护进程后台运行该程序。
*\n       
*\n       
*\n -----------------------------------------------------------------------------------
*\n  版本:     修改人:       修改日期:        描述:
*\n  V0.01    罗先能        2019.7.26    创建
*\n 
***************************************************************************************************/

/**************************************************************************************************
* 头文件
***************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <libgen.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <errno.h>

#include "typedefs.h"
#include "printf.h"


static void usage(void)
{
	printf("read : xxx  rd  <slave_address>  <reg_addr> \n");
	printf("write: xxx  wr  <slave_address>  <reg_addr>  <value> \n\n");
}

int main(int argc, char *argv[])
{
	int ret;
	int fd;
	char buf[16];
	if ((argc != 4) && (argc != 5))
	{
		usage();
		return -1;
	}

	// 读取参数
	if (argc == 4)   
	{
		if (strcmp(argv[1], "rd") != 0)
		{
			dbg_error("strcmp(argv[1], \"rd\")\n");
			return -1;
		}

		fd = open("/dev/jimu.adv7280a", O_RDWR);
		if(fd < 0)
		{
			dbg_error("open(/dev/jimu.adv7280a)");
			return -1;
		}

		memset(buf, 0, sizeof(buf));
		buf[0] = (char)strtol(argv[2], NULL, 0);
		buf[1] = (char)strtol(argv[3], NULL, 0);
		ret = read(fd, buf, 4);
		close(fd);
		if (ret < 0)
		{
			dbg_error("read()==%d\n", ret);
			return -1;
		}
		printf("0x%02x,0x%02x,0x%02x \n", buf[0], buf[1], buf[2]);
		//return 0;
	}


	// 写参数
	if (argc == 5)   
	{
		if (strcmp(argv[1], "wr") != 0)
		{
			dbg_error("strcmp(argv[1], \"wr\")\n");
			return -1;
		}

		fd = open("/dev/jimu.adv7280a", O_RDWR);
		if(fd < 0)
		{
			dbg_error("open(/dev/jimu.adv7280a)");
			return -1;
		}

		memset(buf, 0, sizeof(buf));
		buf[0] = (char)strtol(argv[2], NULL, 0);
		buf[1] = (char)strtol(argv[3], NULL, 0);
		buf[2] = (char)strtol(argv[4], NULL, 0);
		ret = write(fd, buf, 4);
		close(fd);
		if (ret < 0)
		{
			dbg_error("write() == %d\n", ret);
			return -1;
		}
		printf("0x%02x,0x%02x,0x%02x \n", buf[0], buf[1], buf[2]);
		//return 0;
	}

	return 0;
}


