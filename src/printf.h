﻿

#pragma once
#ifndef _PRINTF_8614987F_15CB_485F_81D8_15CE8BCC9F30_H_
#define _PRINTF_8614987F_15CB_485F_81D8_15CE8BCC9F30_H_

#ifdef __cplusplus
extern "C" {
#endif 

#include <stdio.h>
#include <stdint.h>


#define dbg_printf(...)   \
	do{ printf("[%s,%d,%s] ", __FILE__, __LINE__, __func__); printf(__VA_ARGS__); }while(0)

	
#define dbg_info(...)   \
		do{ printf("[%s,%d,%s] ", __FILE__, __LINE__, __func__); printf(__VA_ARGS__); }while(0)
	
#define dbg_warn(...)   \
		do{ printf("[warn,%s,%d,%s] ", __FILE__, __LINE__, __func__); printf(__VA_ARGS__); }while(0)

#define dbg_error(...)   \
	do{ printf("[error,%s,%d,%s] ", __FILE__, __LINE__, __func__); printf(__VA_ARGS__); }while(0)

#define dbg_fatal(...)   \
	do{ printf("[fatal,%s,%d,%s] ", __FILE__, __LINE__, __func__); printf(__VA_ARGS__); }while(0)


#ifdef __cplusplus
}
#endif 
#endif 



