import os
import sys

INCLUDES  = []

cwd = os.getcwd()
list = os.listdir(cwd)
for d in list:
    path = os.path.join(cwd, d)
    if os.path.isfile(os.path.join(path, 'include.py')):
        INCLUDES += Split(path)
        INCLUDES += SConscript(os.path.join(d, 'include.py'))

Return('INCLUDES')
