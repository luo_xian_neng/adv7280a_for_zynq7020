/***********************************************************************************************//**
*\n  @file       types.h
*\n  @brief      基本数据类型定义
*\n  @details
*\n -----------------------------------------------------------------------------------
*\n  文件说明：
*\n       1. 基本数据类型定义
*\n       
*\n -----------------------------------------------------------------------------------
*\n  版本:   修改人:       修改日期:        描述:
*\n  V0.01 罗先能        2019.7.26          创建
*\n 
***************************************************************************************************/
#ifndef __ADAS87E15154_0843_4DA7_9628_BD9232087DA6__
#define __ADAS87E15154_0843_4DA7_9628_BD9232087DA6__


/**************************************************************************************************
* 头文件
***************************************************************************************************/
#include <stdint.h>

/**************************************************************************************************
* 宏定义、结构定义
***************************************************************************************************/
#define u8      uint8_t
#define s8      int8_t

#define u16     uint16_t
#define s16     int16_t

#define u32     uint32_t
#define s32     int32_t

#define u64     uint64_t
#define s64     int64_t

#define f32     float
#define f64     double

// 输出参数
#ifndef OUT
#define OUT
#endif

// 输入/输出参数
#ifndef INOUT
#define INOUT
#endif

// 私有定义
#ifndef PRIVATE
#define PRIVATE  static 
#endif

// 全局定义
#ifndef PUBLIC
#define PUBLIC
#endif

// API外部接口定义
#ifndef API
#define API
#endif



#endif

