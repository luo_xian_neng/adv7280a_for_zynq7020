#//---------------- ���Ŀ¼ -------------------------------------
BIN_DIR    	:= $(shell pwd)/build/bin


RM  := rm -rf

release:
	@scons --file=SConstruct.py || exit $$?
	ls -l $(BIN_DIR)
	@echo '==================================================================='
	@echo '== Finished '
	@echo '==================================================================='
 


clean:
	@scons --file=SConstruct.py -c


all:  clean  release


help:
	@echo "make"
	@echo "make clean"
	@echo "make all"
	@echo "make help"


.PHONY: release  clean  all  help

